# Stears Coding Homework

Thanks for your application to Stears,

This is a homework for Stears’ Subscrybd Technical Team Lead role. We expect that you will use this test to demonstrate your ability to build a real time scalable system.

For this role, we prefer fullstack software engineer candidates with:
- Experience building backend with Nodejs
- Experience building frontend with Reactjs

For each task, you will be given a back story and a couple of tasks to summarise what needs to be done with a strict definition of done. Either complete all tasks or attempt as much as possible so we have enough information to assess your skill & experience.

#### Submissions

- Create a private [gitlab](https://gitlab.com/) repository and add @foluso_ogunlana & @sani_sadiq as maintainers
- Create a README.md with the following:
  - Clear set up instructions (assume no knowledge of the submission or the stack)
  - Notes of any new useful tools / patterns you discovered while completing the test

#### Deadlines

You will be given at least 5 working days to complete the task and your deadline will be communicated via email.

#### Assessment

Our favourite submissions are -

1. **Clean & simple code** - Minimal, high quality and well tested code that gets to the heart of the problem and stops there.
2. **Rigorous** - Complete solutions with well researched applications of the right technology choices.
3. **UX & DevX** - Smooth user experience which is easy to deploy, well documented and easy to contribute to.

Your submission will also impress us by demonstrating one or more of the following -

- **Mastery** in your general use of language, libraries, frameworks, architecture / design / deployment patterns
- **Unique skills** in specific areas - particularly data engineering, devops & testing

#### Conduct

By submitting, you assure us that you have not shared the test with anyone else and that all work submitted is your own. Though you are allowed to use whatever online or offline resources you see fit to learn skills for the tasks.

# **Coding Homework**

**Guidelines**
- Complete all tasks if possible
- Keep your solution minimal and light-weight
- Include a README.md telling us how to use your code
- BE - Using a framework like Express, NestJS or NextJS will save you time. If you don't, provide adequate documentation so we don't lose time figuring out how to run the code.
- FE - Use React or [NextJS](https://nextjs.org/docs) and use modern React patterns, e.g. Hooks, functional components

## Payments

### Background

Streaks is a gaming company that builds web based pay-as-you-go games, and they want to allow customers to pay directly on their site by purchasing a month’s worth of game time. The company is looking to develop an API that accepts any payment platform, and will allow it collect payments from its customers. They plan to integrate several payment platforms in the future, but for now they only need Flutterwave and Paystack.

As a Senior Technical Team Lead, you are to architect and build the system using the following task as a guide.

### Task 

Build a frontend that accepts payments from a user on the Streaks website using test cards in Paystack and Flutterwave.
Also build an API that allows an admin to add or remove payment platforms that the user can choose from (focus on Paystack & Flutterwave only though).

**Note** You don’t need to use webhooks to receive payment transaction success or failure, just rely on the UI to save time.

**Definition of done**:

- [ ] Must allow a user make a payment on screen
- [ ] Must allow a user choose a payment platform to use for a payment from multiple options
- [ ] Must allow a payment platform to be added to or removed from the available options e.g. Paystack, Flutterwave via API call
- [ ] Must be built to support addition of multiple payment platforms outside of Paystack and Flutterwave, although for this exercise, you only need to worry about those two
- [ ] Must allow an admin to enable / disable a payment platform via API call
- [ ] Must allow an admin to modify payment platform details and credentials via API call

## Subscriptions

### Background

Streaks are very pleased with the feature that allows them to successfully add multiple payment platforms and accept payment from them. The Streaks payments product manager realizes that the same customers come back each month to buy the same games when discounted. She proposes that Streaks makes the pivot to a subscription based service. 

### Task

Create a single monthly subscription plan on your application that allows the customer to pay to subscribe using any payment gateway available on site, and update your management API to allow an admin add and remove other subscription plans.

**Definition of done**:

- [ ] Must have a CRUD (Create, Read, Update, Delete) REST API allowing admins to manage subscription plans
- [ ] When a customer pays to subscribe, the system must bill the customer at their end of every billing cycle (only monthly needed)
    - [ ] If a customer subscribes on the 8th of March, they should be billed at the end of the day on the 8th of April, May, June etc…
    - [ ] If a customer subscribes on the 29th, 30th, or 31st of any month, then the customer should be billed on the 28th of the following months
- [ ] Must auto-cancel a subscription if a payment fails after the payment has been retried a configurable number of times
- [ ] Must have an API that will allow for an admin to configure the number of retries for failed payments. E.g. retry 5 times

## Management

### Background

Your subscription feature was a hit. The customers love it and now you have a lot of customers paying for Streak subscriptions. However your team is having trouble scaling the analytics, marketing and customer success because they lack the transparency and control they need over the subscriptions of their users. They need a simple UI to use the admin API you’ve built so they can manage it without engineers.

### Task

Make a management dashboard that visualizes the subscription data, and allows an admin to update data where necessary

#### Hints

- We welcome the use of 3rd party dashboard creators to save time. You only have to meet the acceptance criteria
- You may need to create any outstanding API endpoints to support the dashboard

### Definition of done:

- [ ] Must have an interface that will allow admin to add / remove / disable / enable a payment provider
- [ ] Must have an interface that will allow the admin to add / remove / update subscription plans.
- [ ] Must be able to see a report of transactions & subscriptions for the day, specifying the count of those successful and failed

## Thanks!

If you made it this far, thanks for your time.
We look forward to reviewing your solid application with you!
